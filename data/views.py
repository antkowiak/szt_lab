from django.shortcuts import render
from django.template import Context

# Create your views here.


def page(request):
    return render(request, 'page.html')


def data(request):
    return render(request, 'data.html')


def myname(request):
    context = {
        'firstname': 'Patryk',
        'lastname': 'Antkowiak',
        'index': '113983'
    }
    context = Context(context)
    return render(request, 'myname.html', context=context)
