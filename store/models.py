from django.db import models
from django.utils import timezone

# Create your models here.


class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    description = models.TextField()
    quantity = models.IntegerField(default=0)
    publish_date = models.DateField(default=timezone.now)


class Shoe(models.Model):
    brand = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    description = models.TextField()
    size = models.DecimalField(decimal_places=1, max_digits=3, default=0)
    price = models.DecimalField(decimal_places=2, max_digits=10, default=0)
