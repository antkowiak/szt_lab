from django.contrib import admin
from .models import Book, Shoe

# Register your models here.


class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'publish_date', 'quantity')
admin.site.register(Book, BookAdmin)


class ShoeAdmin(admin.ModelAdmin):
    list_display = ('brand', 'name', 'size', 'price')
admin.site.register(Shoe, ShoeAdmin)
