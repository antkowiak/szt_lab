from django.shortcuts import render
from django.http import HttpResponse
from django.template import Context
from .models import Book

# Create your views here.


def home(request):
    return render(request, 'home.html')


def store(request):
    return render(request, 'store.html')


def books(request):
    context = {
        'booksCount': Book.objects.count(),
        'books': Book.objects.all()
    }
    context = Context(context)

    return render(request, 'books.html', context=context)
